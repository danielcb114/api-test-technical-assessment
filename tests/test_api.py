import unittest
import yaml
import requests
import random
import string
import os

CONFIG_PATH = 'api_test_config.yml'

# How long random titles should be to be unique
RANDOM_STR_LENGTH = 1000


class TestApi(unittest.TestCase):
    def get_blog_post_with_title(self, title):
        """
        Given a title, will return the first post matching that, or None
        :param title:
        :return:
        """
        results = requests.get(self.blog_posts_url).json()
        for result in results:
            if result['title'] == title:
                return result

    def get_author_with_name(self, first_name=None, last_name=None):
        """
        Given a first name and/or last name, will return the first result that matches either or None
        :param first_name:
        :param last_name:
        :return:
        """
        results = requests.get(self.authors_url).json()
        for result in results:
            if result['firstName'] == first_name:
                return result
            if result['lastName'] == last_name:
                return result

    @classmethod
    def setUpClass(cls):
        # e.g. dev, test, prod
        try:
            environment = os.environ['API_DEPLOY_ENV']
        except KeyError:
            # Assume dev if not specified
            environment = 'dev'
        with open(CONFIG_PATH) as fstream:
            cls.config = yaml.full_load(fstream)[environment]

        cls.base_url = cls.config['base_url']
        cls.authors_url = cls.base_url + '/authors'
        cls.blog_posts_url = cls.base_url + '/blog-posts'
        cls.blog_posts_url = cls.base_url + '/blog-posts'

    def test_app_lifecycle1(self):
        # Test the lifecycle of the tool; create author, get author ID, create article, get article ID, update, delete
        random_title = TestUtils.get_random_string()
        # Create author
        firstname = TestUtils.get_random_string()
        data = {
            'firstName': firstname,
            'lastName': 'lastname',
        }
        requests.post(self.authors_url, data=data)
        # Get author ID
        author_id = self.get_author_with_name(first_name=firstname)['id']
        # Update author's lastname
        lastname = TestUtils.get_random_string()
        data['lastName'] = lastname
        requests.put(self.authors_url + f'/{author_id}', data=data)

        # Create article
        data = {
            'title': random_title,
            'body': 'body',
            'author_id': author_id,
            'publishDate': '2019-03-06T15:30:00.000'
        }
        requests.post(self.blog_posts_url, data=data)
        # Get article ID
        article_id = self.get_blog_post_with_title(random_title)['id']
        # Update article
        new_body = 'body2'
        data['body'] = new_body
        requests.put(self.blog_posts_url + f'/{article_id}', data=data)
        # Verify update went through
        article_body = self.get_blog_post_with_title(random_title)['body']
        self.assertEqual(new_body, article_body)
        # Delete article
        requests.delete(self.blog_posts_url + f'/{article_id}')
        # Verify article was deleted
        self.assertIsNone(self.get_blog_post_with_title(random_title))
        # Delete author
        requests.delete(self.authors_url + f'/{author_id}')
        # Verify author was deleted
        self.assertIsNone(self.get_author_with_name(first_name=firstname))

    def test_basic_title_functionality(self):
        # verify title retrieval works; if the below call works with no error the test will pass
        expected = 'Title 1000'
        actual = self.get_blog_post_with_title('Title 1000')['title']
        self.assertEqual(expected, actual)

    def test_basic_author_functionality(self):
        # Verify basic author; if the below calls work with no errors the test will pass
        expected = 'John'
        actual = self.get_author_with_name(last_name='Doe')['firstName']
        self.assertEqual(expected, actual)

        expected = 'Thoreau'
        actual = self.get_author_with_name(first_name='Henry')['lastName']
        self.assertEqual(expected, actual)


class TestUtils:
    """
    Class for assisting in testing (convenience methods, etc.)
    """

    @staticmethod
    def get_random_string(length=RANDOM_STR_LENGTH):
        # Source: https://stackoverflow.com/questions/2257441
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))


if __name__ == '__main__':
    unittest.main()
