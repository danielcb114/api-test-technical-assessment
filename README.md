# Description
This repository was created for a technical assessment. The assignment was to develop a suite of tests for basic blog post CRUD functionality. The Python code in the "tests" directory is a decent overview on some of the practices and preferences I have. 

# How to run
Tests were developed against Python 3.6.8; you can run pip install -r requirements.txt (while in the appropriate
environment) to ensure all of the necessary packages are installed.

Set the environment
From the repository root, run python -m unittest discover

# How to configure URL
api_test_config.yml has the base URL; the environment is set by the "API_DEPLOY_ENV" environment variable; if the
environment variable is not set, it will default to dev.

